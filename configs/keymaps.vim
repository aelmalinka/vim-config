" Copyright 2013 (c) Malina Thomas (malinka) <malinka@entropy-development.com>

" 2017-11-09 AMR TODO: should this be done on v mode
inoremap jk <esc>
vnoremap jk <esc>
nnoremap <esc> :noh<return><esc>
tnoremap <C-j><C-k> <C-\><C-n>

nnoremap <leader>q :quit<cr>
nnoremap <leader>w :write<cr>
nnoremap <leader>a :vnew<cr>
nnoremap <leader>d :vertical split .<cr>
nnoremap <leader>v :vnew<cr>
nnoremap <leader>h :vertical help 
nnoremap <leader>r :edit!<cr>
nnoremap <leader>s :split .<cr>
nnoremap <leader>c :set spell!<cr>

" 2023-07-17 AMR TODO: nvim
" 2022-04-08 AMR TODO: splits when in terminal already
" nnoremap <leader>z :vertical term<cr>
" nnoremap <leader>x :term<cr>
" nnoremap <leader>tz :tab term<cr>

nnoremap <leader>t :tabe .<cr>
nnoremap <leader>te :tabe<cr>

nnoremap <leader>l :ALEToggle<cr>
nnoremap <leader>f :ALEFix<cr>

nnoremap <leader>ec :tabe $MYVIMRC<cr>
nnoremap <leader>tw :set wrap!<cr>

" 2023-06-22 AMR TODO: clipboard?

nnoremap H ^
nnoremap L $
vnoremap H ^
vnoremap L $
