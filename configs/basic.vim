" Copyright 2013 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
" basic options

colorscheme darkblue

filetype plugin on
filetype plugin indent on

syntax on

hi Normal guibg=NONE ctermbg=NONE

set autoindent
set noexpandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set splitright
set splitbelow
set pastetoggle=<F10>

set number
set autoread
set undofile
set undodir=${HOME}/.vim/undo

let mapleader = ","
let localmapleader = "," " 2013-11-06 AMR TODO: research g:mapleader

" 2020-06-17 AMR TODO: don't force wayland clipboard
" 2022-06-03 AMR TODO: this seems wrong
xnoremap "+y y:call system("wl-copy", @")<cr>
nnoremap "+p :let @"=substitute(system("wl-paste --no-newline"), '<C-v><C-m>', '', 'g')<cr>p
nnoremap "*p :let @"=substitute(system("wl-paste --no-newline --primary"), '<C-v><C-m>', '', 'g')<cr>p

" 2019-08-23 AMR TODO: git status
set laststatus=2
set statusline=%F%m%r%h				" 2013-11-06 AMR NOTE: Full path, modified flag, ro flag, help flag
set statusline+=\ %w				" 2013-11-06 AMR NOTE: preview flag
set statusline+=%=					" 2013-11-06 AMR NOTE: switch to right side
set statusline+=\ CWD:\ %{getcwd()}	" 2013-11-06 AMR NOTE: cwd
set statusline+=\ \ \%6l/%6L		" 2013-11-06 AMR NOTE: line numbers
set statusline+=,\ \ %c				" 2015-02-15 AMR NOTE: column position

" 2019-08-23 AMR TODO: netrw histfile?
let g:netrw_list_hide = '\(^\|\s\s\)\zs\.\S\+'

" 2022-05-06 AMR FIXME: C/C++ improvements
" 2022-07-26 AMR FIXME: gtest includes

" 2021-01-20 AMR NOTE: use clippy and check tests
let g:ale_rust_cargo_use_clippy = 1
let g:ale_rust_cargo_check_all_targets = 1
let g:ale_rust_cargo_check_examples = 1
let g:ale_rust_cargo_check_tests = 1

" 2022-06-03 AMR TODO: is this needed by idea?
function! Input(prompt)
	call inputsave()
	let text = input(a:prompt.' ')
	call inputrestore()
	return text
endfunction

let g:ale_linters = {
	\'javascript': ['eslint'],
	\'javascriptreact': ['eslint'],
	\'typescript': ['eslint'],
	\'typescriptreact': ['eslint'],
\}

" 2021-09-13 AMR TODO: rustfmt doesn't seem to work
let g:ale_fixers = {
	\'*': ['remove_trailing_lines', 'trim_whitespace'],
	\'javascript': ['eslint'],
	\'javascriptreact': ['eslint'],
	\'typescript': ['eslint'],
	\'typescriptreact': ['eslint'],
	\'rust': ['rustfmt'],
\}
