" Copyright 2013 (c) Malina Thomas <malina@mthomas.dev>

" 2023-07-31 AMR TODO: fix initials/name references
iabbrev @@ jk"=email<cr>p
iabbrev me@ jk"=name<cr>p
iabbrev lcopy Copyright jk"=strftime("%Y")<cr>pa (c) jk"=name<cr>pa<jk<cr>pa>
iabbrev lic Distributed under the terms of the GNU Affero General Public License v3
iabbrev llic Distributed under the terms of the GNU Lesser General Public License v3
iabbrev clic Distributed under the terms of the Creative Commons Attribution Share-Alike License v4.0
iabbrev glic Distributed under the terms of the GNU General Public License v3
iabbrev 2lic Distributed under the terms of the GNU General Public License v2
iabbrev flic Copying and distribution of this file, with or without modification, are<cr>permitted in any medium without royalty provided the copyright notice<cr>and this notice are preserved. This file is offered as-is, without any<cr>warranty.
iabbrev fixme: jk"=strftime("%F")<cr>pa jk"=initials<cr>pa FIXME:
iabbrev todo: jk"=strftime("%F")<cr>pa jk"=initials<cr>pa TODO:
iabbrev note: jk"=strftime("%F")<cr>pa jk"=initials<cr>pa NOTE:
iabbrev hack: jk"=strftime("%F")<cr>pa jk"=initials<cr>pa HACK:
iabbrev bug: jk"=strftime("%F")<cr>pa jk"=initials<cr>pa BUG:
