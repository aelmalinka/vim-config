" Copyright 2021 (c) Malina Thomas (malinka) <malinka@entropy-development.com>

" 2021-02-26 AMR TODO: clean way to trigger insert leave
"	also a way to force off when in imode?
silent! let l:fcitx_found = system('command -v fcitx-remote')

if l:fcitx_found !~ '\w\+'
	finish
endif

" a clean way to change back and forth inside vim? ,j?
au InsertLeave * :silent !fcitx-remote -c
