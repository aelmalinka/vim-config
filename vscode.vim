" settings/reqs
let name = "Malina Thomas"
let email = "Malina.L.Thomas@associates.cpb.dhs.gov"
let initials = "MLT"

let mapleader = ","
let localmapleader = "," " 2013-11-06 AMR TODO: research g:mapleader

" from keysmaps.vim
inoremap jk <esc>
vnoremap jk <esc>
nnoremap <esc> :noh<return><esc>

nnoremap <leader>q :quit<cr>
nnoremap <leader>w :write<cr>
nnoremap <leader>a :vnew<cr>
nnoremap <leader>d :vertical split .<cr>
nnoremap <leader>v :vnew<cr>
nnoremap <leader>h :vertical help 
nnoremap <leader>r :edit!<cr>
nnoremap <leader>s :split .<cr>
nnoremap <leader>c :set spell!<cr>

nnoremap <leader>l :ALEToggle<cr>
nnoremap <leader>f :ALEFix<cr>

nnoremap <leader>t :tabe .<cr>
nnoremap <leader>te :tabe<cr>

nnoremap <leader>l :ALEToggle<cr>
nnoremap <leader>f :ALEFix<cr>

nnoremap <leader>ec :tabe $MYVIMRC<cr>
nnoremap <leader>tw :set wrap!<cr>

nnoremap H ^
nnoremap L $
vnoremap H ^
vnoremap L $

" from abbrev.vim (do not work)
iabbrev @@ malinka@entropy-development.com
iabbrev me@ Malina Thomas (malinka)
iabbrev lcopy Copyright jk"=strftime("%Y")<cr>pa (c) Malina Thomas (malinka) <malinka@entropy-development.com>
iabbrev lic Distributed under the terms of the GNU Affero General Public License v3
iabbrev llic Distributed under the terms of the GNU Lesser General Public License v3
iabbrev clic Distributed under the terms of the Creative Commons Attribution Share-Alike License v4.0
iabbrev glic Distributed under the terms of the GNU General Public License v3
iabbrev 2lic Distributed under the terms of the GNU General Public License v2
iabbrev flic Copying and distribution of this file, with or without modification, are<cr>permitted in any medium without royalty provided the copyright notice<cr>and this notice are preserved. This file is offered as-is, without any<cr>warranty.
iabbrev fixme: jk"=strftime("%F")<cr>pa AMR FIXME:
iabbrev todo: jk"=strftime("%F")<cr>pa AMR TODO:
iabbrev note: jk"=strftime("%F")<cr>pa AMR NOTE:
iabbrev hack: jk"=strftime("%F")<cr>pa AMR HACK:
iabbrev bug: jk"=strftime("%F")<cr>pa AMR BUG:
