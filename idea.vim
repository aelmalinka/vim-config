" Copyright 2022 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
" idea vim config

" 2022-06-03 AMR TODO: ideavim fails to pic these up
let mapleader = ","
let localmapleader = "," " 2013-11-06 AMR TODO: research g:mapleader

source ~/.vim/configs/keymaps.vim
source ~/.vim/configs/abbrev.vim
source ~/.vim/configs/keymaps_disable.vim
