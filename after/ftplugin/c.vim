" 2013-11-27 AMR TODO: template file thingy

function! ClassHeader(list)
	let l:ret = ''
	let l:count = 0
	for l:name in a:list
		let l:count += 1
		let l:x = 0
		while l:x < l:count
			let l:x += 1
			let l:ret = l:ret . "\t"
		endwhile
		let l:x = 0
		let l:ret = l:ret . "namespace " . l:name . "\n"
		while l:x < l:count
			let l:x += 1
			let l:ret = l:ret . "\t"
		endwhile
		let l:x = 0
		let l:ret = l:ret . "{\n"
	endfor
	let l:y = l:count
	while l:y > 0
		while l:x < l:y
			let l:x += 1
			let l:ret = l:ret . "\t"
		endwhile
		let l:y -= 1
		let l:x = 0
		let l:ret = l:ret . "}\n"
	endwhile
	let l:ret = l:ret . "\n"
	return l:ret
endfunction

inoremap <buffer> honce #if !defined <esc>:let @"=toupper(Input('Header Identification'))<cr>pa<cr>#<tab>define <esc>pa<cr><cr>#endif<esc>ka
inoremap <buffer> nname <esc>:let t=split(Input('Class Definition'))<cr>:let @"=ClassHeader(t)<cr>p<esc>j:execute "normal! " . repeat("jj", len(t))<cr>kko
inoremap <buffer> cguard <cr>#	ifdef __cplusplus<cr>extern "C" {<cr>#	endif<cr><cr><cr>#	ifdef __cplusplus<cr>}<cr>#	endif<cr><esc>kkkkko
