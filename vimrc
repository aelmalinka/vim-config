" Copyright 2013 (c) Malina Thomas (malinka) <malinka@entropy-development.com>

" 2013-12-20 AMR TODO: mru
" 2014-01-27 AMR TODO: edit ftplugin files?
" 2017-11-09 AMR TODO: get to work with VS vim mode plugin?
" 2021-02-26 AMR TODO: actually work in windows?

if has("win32") || has("win64")
	let $VIMDIR=$HOME."/vimfiles"
else
	let $VIMDIR=$HOME."/.vim"
endif

scriptencoding utf-8

if filereadable($VIMDIR."/settings.vim")
	source $VIMDIR/settings.vim
endif

" 2021-02-26 AMR FIXME: does not work if fcitx-remote doesn't exist
" source $VIMDIR/configs/日本語.vim
" 2023-07-31 AMR TODO: does it make sense to have variables for which of these
" to laod?
source $VIMDIR/configs/basic.vim
source $VIMDIR/configs/keymaps.vim
source $VIMDIR/configs/abbrev.vim
source $VIMDIR/configs/keymaps_disable.vim

autocmd! bufwritepost .vimrc source %
autocmd! bufwritepost $VIMDIR/configs/*.vim source %
